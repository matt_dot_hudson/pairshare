# PairShare: HOWTO Share Bluetooth Pairing Between Windows 10 and Linux

## Background

This guide assumes you are booted inte Ubuntu Linux, Windows is installed, its C: volume is mounted on the unifile system, and that Bluetooth devices are previously paired under Windows.

After rebooting from Windows to Linux, device pairing is lost because it depends on keys stored in the Windows registry. This condition is terrible HCI behavior and must be stopped!

## Solution

Install Windows 10 Bluetooth keys in Linux.

### hivexsh
To read Bluetooth keys from the Windows registry the gentle reader is advised to install hivexsh.

```
sudo apt install -y libhivex-bin
```

### Read Bluetooth keys
Edit the 'load' parameter to reflect the path to your SYSTEM registry cluster.

Edit the 'cd' parameter to reflect the directory named for your Bluetooth adapter. If you do not know your Bluetooth MAC yet, it is likely given by the name of the only folder in /var/lib/bluetooth (i.e. /var/lib/bluetooth/48:F1:7F:DD:35:3C). In the Registry path the MAC must appear in lowercase and without punctuation.

```
#!/usr/bin/hivexsh -f

load /home/matt/windisk/Windows/System32/config/SYSTEM

cd ControlSet001\Services\BTHPORT\Parameters\Keys\48f17fdd353c
lsval
```

Execute readBT.sh to verify it reads Bluetooth keys from your registry cluster.

### Store Bluetooth keys
Execute getBTkeys.sh. It will print the output of readBT.sh followed by the preformatted (uppercase and without punctuation) keys as they should appear in 'info' files.

cd to the Bluetooth interface folder and edit the device info files: Update the line starting with "Key=" with the corresponding key from readBT.sh.

```
cd /var/lib/bluetooth/48:F1:7F:DD:35:3C
vim 64:C7:53:ED:B6:52/info
vim 80:4A:14:6A:E4:05/info
```

Then restart Bluetooth service.

```
systemctl restart bluetooth
```

Re-pair Bluetooth peripherals with Settings|Bluetooth. Now your peripherals are paired in both OSes.
